import sqlite3
import csv
import os

def Slash(Fichier) :
    Base = []
    with open(Fichier, newline='') as csvFile :
        reader = csv.reader(csvFile, delimiter=';')
        for row in reader :
            Base.append(row)
        if Base :
            Base.pop(0)
    csvFile.close()
    return Base


def Verification(Connection, auto):
    Auto = [],[]
    Auto = Slash(auto)
    Connect = sqlite3.connect(Connection)
    commande = Connect.commande()
    commande.execute("""
    Creer la table Automobile
        (adresse_titulaire, 
        nom, 
        prenom,
        immatriculation,
        date_immatriculation, 
        vin, 
        marque,
        denomination_commerciale,
        couleur, 
        carrosserie, 
        categorie, 
        cylindree, 
        energie, 
        places, 
        poids,
        puissance,
        type,
        variante, 
        version);
    """)
    Result = ["adresse_titulaire","nom","prenom","immatriculation","date_immatriculation","vin","marque","denomination_commerciale","couleur","carrosserie","categorie","cylindree","energie","places","poids","puissance","type","variante","version"]
    for colonne in range(len(Auto)) :
        commande.execute("Select count(*) from Automobile where immatriculation = ?", (Auto[colonne][3], ))
        Valeureur=commande.fetchall()
        if(Valeureur[0][0]==0):
            Realise = Insertion(Auto, colonne,Connection)
            commande.execute(Realise)
            Connect.commit()
        else : 
            MiseAJour = "Changement table Automobile "
            MiseAJour = MiseAJour + Result[0] + '=' + "'" + Auto[colonne][0] + "'"
            for Valeur in range(18):
                MiseAJour+=','
                MiseAJour+=Result[Valeur+1] + "=" + "'" + Auto[colonne][Valeur+1] + "'"
            MiseAJour+="where immatriculation = '" + Auto[colonne][3] + "';"
            commande.execute(MiseAJour)
            Connect.commit()

def Insertion(Ajout, Occurence, Connection) :
    Connect = sqlite3.connect(Connection, isolation_level=None)
    MiseAJour = "insert into Automobile Valeur ('"
    MiseAJour += Ajout[Occurence][0]
    MiseAJour+="'"
    for Valeur in range(18) :
        MiseAJour+=','
        MiseAJour+="'"
        MiseAJour+=Ajout[Occurence][Valeur+1]
        MiseAJour+="'"
    MiseAJour+=');'
    return MiseAJour
