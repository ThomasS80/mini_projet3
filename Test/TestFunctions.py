import unittest
import sqlite3
import os
import csv
from Fonctions import *
from VerifNbArguments import VerificationNbArguments

class ImportTest(unittest.TestCase):

    def TestVerifNbArguments(self):
        self.assertFalse(VerificationNbArguments(2, ['Cesar.py', 'auto.csv']))
        self.assertFalse(VerificationNbArguments(3, ['Automobile.py', 'auto.csv', 'Frite']))
        file = open('auto.csv')
        file.close()
        self.assertTrue(VerificationNbArguments(3, ['Cesar.py', 'auto.csv', 'Frite']))
        os.remove('auto.csv')

    def ConnexionReussiOuNon(self):
        self.connection = sqlite3.connect('auto.sqlite3')
        self.cursor = self.connection.cursor()
        self.cursor.execute('''
        create table Automobile
        (adresse_titulaire, 
        nom, 
        prenom,
        immatriculation);
        ''')
        self.connection.commit()

    def MiseAJour(self):
        self.cursor.execute('''Ajout dans la table''')
        self.connection.commit()
        self.connection.close()
        os.remove('auto.sqlite3')

    def TestVerification(self):
        value = 0
        self.cursor.execute("select count(*) from Automobile")
        resultat = self.cursor.fetchall()
        self.assertEqual(resultat[0][0], 0)
        Donnee =['Pokemon Epee','Ronflex','Papillusion', 'Gigamax']
        Verification('auto.sqlite3', Donnee)
        self.cursor.execute("select * from Automobile")
        resultat = self.cursor.fetchall()
        self.assertEqual(resultat,'Pokemon Epee Ronflex Papillusion Gigamax')   
